package jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class UserRepository {
    private static final Logger logger = LoggerFactory.getLogger(UserRepository.class);
    private static Connection connection;


    public UserRepository() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            logger.error("Mysql ");
            logger.error(e.getMessage());
        }
        // Setup the connection with the DB
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost/firstdatabase?"
                    + "user=root&password=password");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public List<User> findAll() throws Exception {
        List<User> userList = new LinkedList<>();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement
                .executeQuery("select * from person");
        while (resultSet.next()) {
            User user = new User();
            user.setUserID(resultSet.getInt("personID"));
            user.setName(resultSet.getString("name"));
            user.setLastName(resultSet.getString("lastName"));
            userList.add(user);
        }
        return userList;
    }

    public User insert(User user) {

        try (PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO person (personID, name, lastName) VALUES (?,?,?);")) {
            connection.setAutoCommit(false);
            int id = getMaxId() + 1;
            user.setUserID(id);
            preparedStatement.setInt(1, id);
            preparedStatement.setString(2, user.getName());
            preparedStatement.setString(3, user.getLastName());
            logger.info(preparedStatement.toString());
            preparedStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        }

        return user;
    }

    private int getMaxId() throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT personID FROM person Order by personID DESC limit 1;");
        if (resultSet.next()) {
            int personID = resultSet.getInt("personID");
            return personID;
        } else return 0;
    }

    public void update(User user, int id) {
        try (PreparedStatement preparedStatement = connection.prepareStatement
                ("Update person Set name=?,lastName=?  where personID=?;")) {
            connection.setAutoCommit(false);

            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getLastName());

            preparedStatement.setInt(3, id);
            logger.info(preparedStatement.toString());
            preparedStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            try {
                connection.rollback();
            } catch (SQLException exception) {
                logger.error(exception.getMessage());
                exception.printStackTrace();
            }
        }
    }

    public void deleteWithId(int id) {
        try (PreparedStatement preparedStatement = connection.prepareStatement("Delete from person where personID=? ")) {
            connection.setAutoCommit(false);
            preparedStatement.setInt(1, id);
            logger.info(preparedStatement.toString());
            preparedStatement.execute();
            connection.commit();
        } catch (SQLException exception) {
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void deleteWithName(String name) {
        try (PreparedStatement preparedStatement = connection.prepareStatement("Delete from person where name=? ")) {
            connection.setAutoCommit(false);
            preparedStatement.setString(1, name);
            logger.info(preparedStatement.toString());
            preparedStatement.execute();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        }
    }

    public User findById(int id) throws SQLException {
        User user = new User();
        Statement statement = null;

        statement = connection.createStatement();


        ResultSet resultSet = statement
                .executeQuery("select * from person where personID=" + id + ";");
        resultSet.next();
        user.setUserID(resultSet.getInt("personID"));
        user.setName(resultSet.getString("name"));
        user.setLastName(resultSet.getString("lastName"));


        return user;
    }
}

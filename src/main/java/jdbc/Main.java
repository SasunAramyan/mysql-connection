package jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        UserRepository userRepository = new UserRepository();
        User user = new User();
        user.setUserID(8);
        user.setName("aabb");
        user.setLastName("cccddaaa");
        userRepository.update(user, user.getUserID());

    }
}
//    public static void main(String[] args) throws Exception {
//        UserRepository userRepository = new UserRepository();
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("showAll --> Show all users ");
//        System.out.println("Create --> Create user ");
//        System.out.println("Edit --> Edit user");
//        System.out.println("DeleteUserWithId -->Delete user with id");
//        System.out.println("DeleteUserWithName -->Delete user with name");
//        System.out.println("Exit --> exit");
//        while (true) {
//            String command;
//            command = scanner.nextLine();
//            logger.info("command -->{}", command);
//            switch (command) {
//                case "help":
//                    System.out.println("showAll --> Show all users ");
//                    System.out.println("Create --> Create user ");
//                    System.out.println("Edit --> Edit user");
//                    System.out.println("DeleteUserWithId -->Delete user with id");
//                    System.out.println("DeleteUserWithName -->Delete user with name");
//                    System.out.println("Exit --> exit");
//                    break;
//                case "showAll":
//                    List<User> userList = userRepository.findAll();
//                    System.out.println(userList);
//                    break;
//                case "Create":
//                    User user = new User();
//                    System.out.print("Type user Name-->");
//                    user.setName(scanner.nextLine());
//                    System.out.print("Type user LastName-->");
//                    user.setLastName(scanner.nextLine());
//                    System.out.print("Type user professionID-->");
//                    user.setProfessionID(scanner.nextInt());
//                    userRepository.insert(user);
//                    break;
//                case "Edit":
//                    User user1 = new User();
//                    System.out.print("Type user Name-->");
//                    user1.setName(scanner.nextLine());
//                    System.out.print("Type user LastName-->");
//                    user1.setLastName(scanner.nextLine());
//                    System.out.print("Type user professionID-->");
//                    user1.setProfessionID(scanner.nextInt());
//                    System.out.print("insert userID-->");
//                    int id = scanner.nextInt();
//                    user1.setUserID(id);
//                    userRepository.update(user1, user1.getUserID());
//                    break;
//                case "DeleteUserWithId":
//                    System.out.print("Insert ID-->");
//                    userRepository.deleteWithId(scanner.nextInt());
//                    break;
//                case "DeleteUserWithName":
//                    System.out.println("Insert Name-->");
//                    userRepository.deleteWithName(scanner.nextLine());
//                    break;
//                case "Exit":
//                    return;
//                default:
//                    System.out.print("Your command is wrong: If you need help insert -->help");
//                    break;
//            }
//        }
//    }

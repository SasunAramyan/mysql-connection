package servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import jdbc.User;
import jdbc.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.SQLException;


@WebServlet(name = "Servlet")
public class UserServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(UserServlet.class);
    private ObjectMapper objectMapper;
    private UserRepository userRepository;

    public UserServlet() {
        objectMapper = new ObjectMapper();
        userRepository = new UserRepository();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String body = getBody(request);
        logger.debug("body: {}", body);

        User user = objectMapper.readValue(body, User.class);

        userRepository.insert(user);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String userJson = objectMapper.writeValueAsString(user);
        response.getWriter().write(userJson);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.debug("do get");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String pathInfo = request.getPathInfo();
        int userId = Integer.parseInt(pathInfo.split("/")[1]);
        try {
            User user = userRepository.findById((userId));

            String userJson = objectMapper.writeValueAsString(user);
            response.getWriter().write(userJson);
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }


    public static String getBody(HttpServletRequest request) throws IOException {

        String body = null;
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;

        try {
            InputStream inputStream = request.getInputStream();
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                char[] charBuffer = new char[128];
                int bytesRead = -1;
                while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                    stringBuilder.append(charBuffer, 0, bytesRead);
                }
            } else {
                stringBuilder.append("");
            }
        } catch (IOException ex) {
            throw ex;
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ex) {
                    throw ex;
                }
            }
        }

        body = stringBuilder.toString();
        return body;
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        String pathInfo = request.getPathInfo();
        int id = Integer.parseInt(pathInfo.split("/")[1]);
        userRepository.deleteWithId(id);
        response.getWriter().write("Person Deleted with id" + id);

    }


    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String body = getBody(request);
        User user = objectMapper.readValue(body, User.class);
        int userID = user.getUserID();
        userRepository.update(user, userID);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        String userJson = objectMapper.writeValueAsString(user);
        response.getWriter().write(userJson);


    }


}

package hibernate;

import hibernate.firstdatabas.Person;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class Application {
    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.getTransaction().begin();
        Person person = createPerson();
        List from_person_ = session.createQuery("FROM Person ").getResultList();
        session.save(person);
        session.getTransaction().commit();
//        Query<Person> select_p_from_person_p = session.createQuery("From Person ");
        session.close();
        System.out.println(from_person_);

    }

    private static Person createPerson() {
        Person person = new Person();
        person.setName("Sasun");
        person.setLastName("Aramyan");

        return person;
    }
}

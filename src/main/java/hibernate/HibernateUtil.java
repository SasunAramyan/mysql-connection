package hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try {
            SessionFactory sf = new Configuration().configure().buildSessionFactory();
            return sf;


        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("There was an error building factory");
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}

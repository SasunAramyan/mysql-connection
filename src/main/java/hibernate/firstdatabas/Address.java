package hibernate.firstdatabas;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Address {


    @Column(name = "city")
    private String city;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public long getHouseID() {
        return houseID;
    }

    public void setHouseID(long houseID) {
        this.houseID = houseID;
    }

    @Column(name = "houseID")
    private long houseID;

}

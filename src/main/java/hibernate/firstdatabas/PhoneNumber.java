package hibernate.firstdatabas;

import javax.persistence.*;

@Entity
@Table(name = "phonenumber")
public class PhoneNumber {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int phoneID;

    @Column(name = "phonenumber")
    private int phonenumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "personID", nullable = false)
    private Person person;


    public int getPhoneID() {
        return phoneID;
    }

    public void setPhoneID(int phoneID) {
        this.phoneID = phoneID;
    }

    public int getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(int phonenumber) {
        this.phonenumber = phonenumber;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}

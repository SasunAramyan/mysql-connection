package hibernate.firstdatabas;

import javax.persistence.*;

@Entity
@Table(name = "role")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "roleID")
    private int roleID;

    @Column(name = "role", unique = true)
    private String role;

    public Role getRoleUser() {
        Role role = new Role();
        role.setRole("User");
        setRoleID(1);
        return role;
    }

    public Role getRoleAdmin() {
        Role role = new Role();
        role.setRole("Admin");
        setRoleID(2);
        return role;
    }


    public int getRoleID() {
        return roleID;
    }

    public void setRoleID(int roleID) {
        this.roleID = roleID;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}

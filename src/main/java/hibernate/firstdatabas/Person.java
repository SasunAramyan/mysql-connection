package hibernate.firstdatabas;


import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "person")
//Annotations put in get methods:
//@Access(value = AccessType.PROPERTY)
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "personID")
    private int personID;

    @Column(name = "name", nullable = false, updatable = false)
    private String name;

    @Column(name = "lastName")
    private String lastName;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "personID"),
            inverseJoinColumns = @JoinColumn(name = "roleID"))
    private Set<Role> roleSet;

    public Set<Role> getRoleSet() {
        return roleSet;
    }

    public void setRoleSet(Set<Role> roleSet) {
        this.roleSet = roleSet;
    }
//    @Column(name = "professionID", updatable = false)
//    private int professionID;
//    @Transient
//    private boolean isMered;
//    @Embedded
//    private Address address;

//    @ElementCollection
//    @CollectionTable(name = "office", joinColumns = @JoinColumn(name = "officeID"))
//    @Column(name = "officeName")
//    private List<String> office = new ArrayList<>();
//
//    @OneToMany(fetch = FetchType.LAZY, mappedBy = "person")
//    List<PhoneNumber> phoneNumberList;


    public int getPersonID() {
        return personID;
    }

    public void setPersonID(int personID) {
        this.personID = personID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    //
    @Override
    public String toString() {
        return "Person{" +
                "personID=" + personID +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", roleSet=" + roleSet +
                '}' + '\n';
    }
}



